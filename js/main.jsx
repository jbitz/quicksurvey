// Enumeration for currently viewed survey screen
const SCREEN_DIRECTORY_SELECT = 0;
const SCREEN_ID_INPUT = 1;
const SCREEN_DIRECTIONS = 2;
const SCREEN_QUESTIONS = 3;
const SCREEN_SAVE_RESPONSES = 4;
const SCREEN_FINISH_SURVEY = 5;

/*
 * Root view of the whole application.
 * Handles switching between the different pages of the survey,
 * as well as file I/O operations
 */
function MainView() {
    // Reference to the survey_data directory containing all application data
    const mainDirectoryHandle = React.useRef(null);
    const setDirectoryHandle = (handle) => {
        mainDirectoryHandle.current = handle;
    };

    // Dictionary storing responses to each survey question (keyed by image file name)
    const [surveyResponses, setSurveyResponses] = React.useState({})
    const recordResponse = (name, response) => {
        const responsesCopy = JSON.parse(JSON.stringify(surveyResponses));
        responsesCopy[name] = response;
        setSurveyResponses(responsesCopy);
    }

    // Function used by child components to load an image out of the data directory
    const loadImage = async (filename) => {
        let imageData;
        try {
            const imagesDir = await mainDirectoryHandle.current.getDirectoryHandle('images');
            const imageFile = await imagesDir.getFileHandle(filename);
            const readable = await imageFile.getFile();

            // Convert the FileReader's onload callback into an async function
            const readFileAsync = async (file) => {
                return new Promise((resolve, reject) => {
                    let reader = new FileReader();
                    reader.onload = () => {
                        resolve(reader.result);
                    };

                    reader.onerror = reject;
                    reader.readAsDataURL(file);
                })
            }
            imageData = await readFileAsync(readable);
        } catch (exception) {
            console.log(exception);
            return null;
        }
        return imageData;
    }

    // Writes out the response data to a file named after the current user
    const saveResponseData = async () => {
        try {
            const responsesDir = await mainDirectoryHandle.current.getDirectoryHandle('responses');
            const outputHandle = await responsesDir.getFileHandle(`${userID}.json`, { create: true });
            const outFile = await outputHandle.createWritable();
            await outFile.write(JSON.stringify(surveyResponses));
            await outFile.close();
        } catch (exception) {
            console.log(exception);
            return null;
        }
    }

    const [surveyData, setSurveyData] = React.useState([]); // Entire config.json file contents
    const [userID, setUserID] = React.useState(-1); // -1 for "no user yet"

    const [surveyState, setSurveyState] = React.useState(0); // Current screen to show

    let content;
    if (surveyState == SCREEN_DIRECTORY_SELECT) {
        content = <LoadSurveyDataPage setSurveyData={setSurveyData} setSurveyState={setSurveyState} setDirectoryHandle={setDirectoryHandle} />;
    } else if (surveyState == SCREEN_ID_INPUT) {
        content = <GetUserIDPage surveyData={surveyData} setSurveyState={setSurveyState} setUserID={setUserID} />;
    } else if (surveyState == SCREEN_DIRECTIONS) {
        content = <DirectionsPage setSurveyState={setSurveyState} />;
    } else if (surveyState == SCREEN_QUESTIONS) {
        content = <SurveyQuestionPage
                    userID={userID}
                    surveyData={surveyData[userID]}
                    loadImage={loadImage}
                    setSurveyState={setSurveyState}
                    recordResponse={recordResponse}
                    />;
    } else if (surveyState == SCREEN_SAVE_RESPONSES) {
        content = <SaveSurveyPage 
                    surveyResponses={surveyResponses} 
                    userID={userID} 
                    saveResponseData={saveResponseData} 
                    setSurveyState={setSurveyState}
                    />;
    } else if (surveyState == SCREEN_FINISH_SURVEY) {
        content = <FinishPage />
    } else {
        content = <h1>Invalid survey state!</h1>
    }

    return (
        <React.Fragment>
            <PageHeader />
            <div className="main-content">
                {content}
            </div>
        </React.Fragment>
    );
}

/*
 * Handles requesting permission to access the user's file system, along with loading
 * all the necessary configuration files for the survey
 */
function LoadSurveyDataPage(props) {
    // Use the File System Access API to load the data directory for the survey
    const loadDirectory = async () => {
        // Ask the user to give us acceess to the survey_data directory
        const directoryHandle = await window.showDirectoryPicker();
        if (!directoryHandle) { // User closed the dialog box or it otherwise failed
            return;
        }

        // Make sure we can read the configuration data
        let configData;
        try {
            const configFile = await directoryHandle.getFileHandle('config.json');
            const readable = await configFile.getFile();
            const fileContents = await readable.text();
            configData = JSON.parse(fileContents);
        } catch (exception) {
            console.log(exception);
            return;
        }

        //TODO: Validate images directory exists

        // If all necessary files were present, update the parent component and move to the next stage
        props.setSurveyData(configData);
        props.setSurveyState(SCREEN_ID_INPUT);
        props.setDirectoryHandle(directoryHandle);
    };

    return (
        /*
        TODO: Get a macOS screenshot of the file picker
        TODO: Give a warning if they're not using Chrome
        */
        <React.Fragment>
            <h1> AI-Assisted Reader Study </h1>
            <p className="question-text">
                In this study, you will answer a series of questions in which you provide diagnoses of skin diseases based on various images.
            </p>
            <p className="question-text">
                To begin, please click the "Select Data" button below and navigate to the <span className="filename">survey_data</span> folder in Box.
                Once you have selected the <span className="filename">survey_data</span> folder, press "Select" to load the survey. 
            </p>

            <p className="question-text">
                <b>Note:</b> When you press "Select", Google Chrome will ask you to allow this page to access the folder you choose. Please select "View Files" when shown this prompt.
            </p>
            <button onClick={loadDirectory}>Select Data</button>
        </React.Fragment>
    );
}

/*
 * Handles asking for and validating the user's ID code
 * TODO: User's name should probably go in the config file, for validation purposes
 */
function GetUserIDPage(props) {
    const [idInput, setIdInput] = React.useState("");
    const [errorMessage, setErrorMessage] = React.useState("");

    // Validate that we have dat afor the user, and if so, move on to the directions slide
    const onNextClicked = () => {
        const idNum = parseInt(idInput);
        if (!(idNum in props.surveyData)) {
            setErrorMessage("ID number not found. Please check that you entered the number you received from the study organizer.");
            return;
        }
        props.setUserID(idNum);
        props.setSurveyState(SCREEN_DIRECTIONS);
    };

    return (
        <React.Fragment>
            <h1>Survey Setup</h1>
            <p className="question-text">
                Enter your participant ID number in the box below.
                You should have received this ID number from the study organizer.
            </p>
            <input type="text" value={idInput} onChange={(event) => { setIdInput(event.target.value); }}></input>
            <button onClick={onNextClicked}>Next</button>
            {errorMessage && 
                <div className="error-message">
                    <b>Error: </b>
                    {errorMessage}
                </div>
            }
        </React.Fragment>
    );
}

/*
 * Simple page showing the directions for completing the survey.
 */
function DirectionsPage(props) {
    return (
        <React.Fragment>
            <h1>Diagnosis Questions</h1>
            <p className="question-text">
                On the following pages, you will be presented with images of various skin conditions.
                Initially, you will be asked to diagnose the condition based on the image. 
                <b>It is alright if your diagnosis is not certain.</b>
            </p>

            <p className="question-text">
                You may be asked to answer additional questions about your diagnosis. 
                <b>You may not revise your answers once you submit them</b>, so please think through each carefully before clicking "Next".
            </p>
            <button onClick={() => { props.setSurveyState(SCREEN_QUESTIONS); }}>Next</button>
        </React.Fragment>
    );
}

/* 
 * Shows the actual survey questions and keeps track of which image the user is currently viewing/evaluating
 * For specifics on how the questions are implemented, see survey_question.jsx
 */
function SurveyQuestionPage(props) {
    const [curQuestion, setCurQuestion] = React.useState(0); // Index into the array of images to show

    const nextQuestion = () => {
        const nextQuestion = curQuestion + 1;
        // If we've finished all the questions
        if (nextQuestion == props.surveyData.image_names.length) {
            props.setSurveyState(SCREEN_SAVE_RESPONSES); // Advance to next part of the survey
        } else {
            setCurQuestion(curQuestion + 1);
        }
    };

    return (
        <React.Fragment>
            <h1>Question {curQuestion + 1}</h1>
            <SurveyQuestion
                imageName={props.surveyData.image_names[curQuestion]}
                questionsToShow={props.surveyData.questions}
                loadImage={props.loadImage}
                nextQuestion={nextQuestion}
                recordResponse={props.recordResponse}
            />
        </React.Fragment>
    );
}

/*
 * Page which prompts the user to save their survey data to the the survey_data/responses directory
 * Chrome will pop up a warning when we try to do this, so we should let them know beforehand.
 */
function SaveSurveyPage(props) {
    const onNextClicked = async () => {
        await props.saveResponseData();
        props.setSurveyState(SCREEN_FINISH_SURVEY);
    }

    return (
        <React.Fragment>
            <h1> Final Steps </h1>
            <p className="question-text">
            Please click the button below to save your results. You will receive a prompt from Chrome to save the file - accept it to allow the web browser to save the file to Box.
            </p>
            <button onClick={onNextClicked}>Next</button>
        </React.Fragment>
    )
}

/* 
 * Final page of the survey - thank the user for participating and tell them to leave
 */
function FinishPage(props) {
    return (
        <React.Fragment>
            <h1> Survey Complete </h1>
            <p className="question-text">
                Thank you for completing the survey! Your time and assistance is appreciated. 
            </p>
            <p className="question-text">
                You may now close your browser window.
            </p>
        </React.Fragment>
    );
}

/*
 * Component for the Stanford banner at the top of the page
 */
function PageHeader() {
    return (
        <div className="page-header">
            <img id="header-image"></img>
        </div>
    )
}