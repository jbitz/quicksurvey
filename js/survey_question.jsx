// Dictionary of all values which the survey might fill (depending on the configuration for that user)
const DEFAULT_SURVEY_ANSWERS = {
    diagnosis: "",
    confidence: "",
    followUp: "",
}

/* 
 * Component controlling a single page (i.e. all questions attached to one image) of the survey
 */
function SurveyQuestion(props) {
    const [imageData, setImageData] = React.useState(null); // Primary image to show on left of screen
    React.useEffect(() => {
        //useEffect can't take an async function, so create one and then just call it immediately
        const getImage = async () => {
            const loadedData = await props.loadImage(props.imageName);
            setImageData(loadedData);
        };
        getImage();
    }, [props.imageName]);

    const [answers, setAnswers] = React.useState(DEFAULT_SURVEY_ANSWERS); // Collected answers for this image
    const [curComponent, setCurComponent] = React.useState(0); // Which sub-question the user is currently answering

    const questionComponents = []; // List of all sub-questions to render
    let componentIdx = 0; // Track what index we're on as we go through possible components to render

    // Render question components
    // Hide them if we haven't reached them yet
    // Show them (but disable changes) if they've already been answered
    if (props.questionsToShow.diagnosis && componentIdx <= curComponent) {
        const setDiagnosis = (val) => {
            setAnswers({ ...answers, diagnosis: val })
        };
        questionComponents.push(<DiagnosisQuestion setValue={setDiagnosis} diagnosis={answers.diagnosis} enabled={componentIdx === curComponent} key="diagnosis" />);
        componentIdx++;
    }
    if (props.questionsToShow.confidence && componentIdx <= curComponent) {
        const setConfidence = (val) => {
            setAnswers({ ...answers, confidence: val })
        };
        questionComponents.push(<ConfidenceQuestion setValue={setConfidence} enabled={componentIdx === curComponent} key="confidence" />);
        componentIdx++;
    }
    if (props.questionsToShow.follow_up && componentIdx <= curComponent) {
        const setFollowUp = (val) => {
            setAnswers({ ...answers, followUp: val })
        };
        questionComponents.push(<FollowUpQuestion setValue={setFollowUp} followUp={answers.followUp} enabled={componentIdx === curComponent} key="followUp" />);
        componentIdx++;
    }

    // Advance to the next sub-question
    const onNextClicked = () => {
        const newCurComponent = curComponent + 1;
        // If we've finished all the questions, advance to the next image
        const numQuestions = Object.values(props.questionsToShow).reduce((acc, cur) => acc + cur);
        if (newCurComponent === numQuestions) {
            props.recordResponse(props.imageName, answers);
            props.nextQuestion();
            setCurComponent(0);
            setAnswers(DEFAULT_SURVEY_ANSWERS);
            setImageData(null);

        } else {
            setCurComponent(newCurComponent);
        }
    }

    //TODO: Add click to zoom for the image
    return (
        <div className="question-container">
            <div className="question-column">
                {imageData && <ZoomableImage src={imageData}/>}
            </div>
            <div className="question-column">
                {questionComponents}
                <button onClick={onNextClicked}>Next</button>
            </div>
        </div>
    );
}

/* 
 * Sub-question: Based on this image, what is your diagnosis? (text response)
 */
function DiagnosisQuestion(props) {
    return (
        <React.Fragment>
            <p className="question-text">
                Based on this image, what is your diagnosis?
            </p>
            <input 
                type="text" 
                value={props.diagnosis} 
                onChange={(e) => { props.setValue(e.target.value); }} 
                disabled={!props.enabled}
                className="long"
            >
            </input>
        </React.Fragment>
    );
}

/* 
 * Sub-question: How confident are you in your diagnosis? (radio buttons with values 0-4)
 */
function ConfidenceQuestion(props) {
    return (
        <React.Fragment>
            <p className="question-text">
                How confident are you in your diagnosis?
            </p>
            <div onChange={(e) => { props.setValue(e.target.value); }}>
                <p>
                    <input type="radio" id="conf0" value="0" name="confidence" disabled={!props.enabled} /> 
                    <label htmlFor="conf0">Not at all confident</label>
                </p>
                <p>
                    <input type="radio" id="conf1" value="1" name="confidence" disabled={!props.enabled} /> 
                    <label htmlFor="conf1">Largely unconfident</label>

                </p>
                <p>
                    <input type="radio" id="conf2" value="2" name="confidence" disabled={!props.enabled} />
                    <label htmlFor="conf2">Middling confidence</label>
                </p>
                <p>
                    <input type="radio" id="conf3" value="3" name="confidence" disabled={!props.enabled} /> 
                    <label htmlFor="conf3">Mostly confident</label>
                </p>
                <p>
                    <input type="radio" id="conf4" value="4" name="confidence" disabled={!props.enabled} /> 
                    <label htmlFor="conf4">Very confident</label>
                </p>
            </div>
        </React.Fragment>
    );
}

/* 
 * Sub-question: What follow-up would you recommend for the patient? (text response)
 */
function FollowUpQuestion(props) {
    return (
        <React.Fragment>
            <p className="question-text">
                What follow-up would you recommend for the patient?
            </p>
            <textarea value={props.followUp} onChange={(e) => { props.setValue(e.target.value); }} disabled={!props.enabled}></textarea>
        </React.Fragment>
    );
}