# Stanford Derm Survey
This is a tool developed for a research survey in conjunction with Stanford Medicine's dermatology department.

**Warning:** Due to the nature of the file I/O APIs used, this software will only work properly when run under Google Chrome, Microsoft Edge, or Opera. Firefox and Safari are currently not supported (see [the API documentation on MDN](https://developer.mozilla.org/en-US/docs/Web/API/FileSystemWritableFileStream#browser_compatibility)).

## Setup

For ease of development, the code is split up into separate HTML, CSS, and JS files, like any sane project. However, since the goal is to produce a single file which can be distributed to users via Box, we include a script to bundle it all into one large HTML file. To download the code and create this file, run:

```sh
$ git clone https://gitlab.com/jbitz/quicksurvey.git
$ cd quicksurvey
$ python build.py > app.html
```
This will produce a self-contained output file `app.html` , which can be distributed to end users.
## Directory Structure
When users open `app.html`, they are prompted to select a `survey_data` directory. This directory should be uploaded on Box (or anywhere else the user can mount on their local file system). It must contain the following files and subdirectories;
```sh
survey_data
|- config.json
|- images (subdirectory)
|	|- img1.png
|	|- img2.png
|	|- (etc.)
|
|- responses (subdirectory)
|	|- (empty by default)
```
The files in the `images` directory can be named whatever you want, as long as they are specified in `config.json` (see below). The `responses` directory will hold a collection of JSON files containing the responses from each user, named by their user IDs.

## `config.json`

A sample `config.json` is shown below. The root object is an array which contains an object describing the survey given to each user. In addition to the user's ID number, it describes which questions they should be given in the survey, as well as which images they should be shown (these must all be files in the `images` subdirectory described above).

```json
[	
	{
		"id": 0,
		"questions": {
			"diagnosis": true,
			"confidence": true,
			"follow_up": true,
			"redo_with_classifier": false,
			"region_of_interest": false
		},
		"image_names": ["img1.jpg", "img2.jpg", "img3.jpg"]
	},
	{
		"id": 1,
		"questions": {
			"diagnosis": true,
			"confidence": true,
			"follow_up": true,
			"redo_with_classifier": true,
			"region_of_interest": true
		},
		"image_names": ["img2.jpg", "img3.jpg", "img4.jpg"]
	}
]
```