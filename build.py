"""
Usage: python build.py > app.html
Combines all the resources needed to run the application into a single html file
    - main_page.html is the root HTML document

    - *.js and *.jsx files should be under the 'js' directory. They will be substituted
      in for the line {% SCRIPTS %} in main_page.html

    - *.css files should be under the 'css' directory. They will be substituted in for
      the line {% STYLES %} in main_page.html

    - Image files should be under the 'img' directory. They will be converted to Base64
      data URLs and included in the {% STYLES %} section. For each image, a new CSS selector
      will be created based on its file name. For example, 'img/header-image.png' will get a
      CSS rule of the form

      #header-image {
        content: url('...');
      }
      
      To use the images in the site, just make sure the <img> tag has an id attribute
      corresponding to this name.

Output from this program should be routed to a new output file, as in python build.py > app.html
"""
import os
import base64

# Convert images to Base64 data URLs so they can be embedded as CSS
def img_to_base_64(filepath):
    binary_file = open(filepath, 'rb').read()  # fc aka file_content
    base64_encoded = base64.b64encode(binary_file).decode('utf-8')
    extension = filepath.split('.')[-1]
    return f'data:image/{extension};base64,{base64_encoded}'

# List all files in a given directory
def get_files_in_dir(dir_path, allowed_extensions, full_path=True):
    if full_path:
        return [f'{dir_path}/{fname}' for fname in os.listdir(dir_path) if fname.split('.')[-1] in allowed_extensions]
    else:
        return [fname for fname in os.listdir(dir_path) if fname.split('.')[-1] in allowed_extensions]

# Print out the contents of each file in the list to the console
# (so that it can be routed to an output file via '>')
def print_files(file_list):
    for f in file_list:
        sub_content = open(f, 'r')
        for line in sub_content:
            print(line, end='')
        print()

# Print out the Base64 encodings of every image in the list
# the file_list should be just the file names, not the full paths
def print_img_css(dir_path, file_list):
    for fname in file_list:
        print('#' + ''.join(fname.split('.')[:-1]) + ' {')
        print('\tcontent: url(', end='')
        print(img_to_base_64(f'{dir_path}/{fname}'), end='')
        print(');')
        print('}')

if __name__ == '__main__':
    main_page = open('main_page.html', 'r')
    # Iterate through each line of the main HTML document, making appropriate substitutions as we go
    for line in main_page:
        # Sub in all JS files
        if line.strip() == '{% SCRIPTS %}':
            print('<script type="text/jsx">')
            jsx_files = get_files_in_dir(dir_path='js', allowed_extensions=('js', 'jsx'))
            print_files(jsx_files)
            print('</script>')
        # Sub in all the CSS and image files
        elif line.strip() == '{% STYLES %}':
            print('<style>')
            css_files = get_files_in_dir(dir_path='css', allowed_extensions=('css'))
            print_files(css_files)

            img_files = get_files_in_dir(dir_path='img', allowed_extensions=('png', 'jpg', 'jpeg'), full_path=False)
            print_img_css(dir_path='img', file_list=img_files)
            print('</style>')
        else:
            print(line, end='')
    print()